namespace EPAY.API.DTO
{
    public class GetCompanyBankResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }
}