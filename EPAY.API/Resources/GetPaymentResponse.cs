using System;

namespace EPAY.API.DTO
{
    public class GetPaymentResponse
    {
        public string Id { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ProcessedAt { get; set; }
        public long Amount { get; set; }
        public string CartId { get; set; }
        public string Status { get; set; }
        public string CompanyName { get; set; }
        public string BankName { get; set; }
        public string IFU { get; set; }
        public string AccountToDebit { get; set; }


    }
}