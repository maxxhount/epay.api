﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EPAY.API.Resources
{
    public class AuthenticateUserResource
    {
        [Required]
        [JsonProperty(PropertyName ="client_id")]
        public string ClientId { get; set; }
        [Required]
        [JsonProperty(PropertyName = "client_secret")]
        public string ClientSecret { get; set; }
    }

    public class RefreshTokenResource
    {
        [JsonProperty(PropertyName = "token")]
        public string ExpiredToken { get; set; }
    }
}
