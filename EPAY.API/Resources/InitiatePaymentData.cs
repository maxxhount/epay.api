using System.ComponentModel.DataAnnotations;

namespace EPAY.API.DTO
{
    public class InitiatePaymentData
    {
        [Required]
        public string IFU { get; set; }
        [Required]
        public long Amount { get; set; }
        [Required]
        public string CartId { get; set; }
        [Required]
        public int BankId { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}