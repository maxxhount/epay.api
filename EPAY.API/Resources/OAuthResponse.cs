﻿using System;
using Newtonsoft.Json;

namespace EPAY.API.Resources
{
    public class OAuthResponse
    {
        [JsonProperty(PropertyName = "access_token")]
        public string Token { get; set; }
        [JsonProperty(PropertyName = "token_type")]
        public string Type { get; set; } = "bearer";
        [JsonProperty(PropertyName = "expires_in")]
        public long ExpireDate { get; set; }
    }
}
