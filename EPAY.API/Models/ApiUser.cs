namespace EPAY.API.Models
{
    public class ApiUser
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}