﻿using System;
using System.Threading.Tasks;
using EPAY.Core.Models;
using EPAY.Data;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EPAY.API.Helpers
{
    public class ApiLogManager
    {
        private readonly ApiLogDbContext _apiDbContext;
        private readonly ILogger _logger;
        public ApiLogManager(ApiLogDbContext context, ILogger<ApiLogManager> logger)
        {
            _apiDbContext = context;
            _logger=logger;
        }

        public async Task CreateLogAsync(object input,object output,string ipAddress, string url,string method, string status,string addedBy)
        {
            try
            {
                ApiLog log = new ApiLog()
                {
                    AddedBy=addedBy,
                    AddedAt = DateTime.Now,
                    Input = JsonConvert.SerializeObject(input),
                    Response = JsonConvert.SerializeObject(output),
                    ClientIP = ipAddress,
                    Url = url,
                    Method = method,
                    Status = status
                };
                await _apiDbContext.ApiLogs.AddAsync(log);
                await _apiDbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Cannot create logs in sqlite db",ex);
            }
        }
    }
}
