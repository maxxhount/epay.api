using Microsoft.AspNetCore.Mvc;
using EPAY.Data;
using EPAY.Core.Models;
using Microsoft.Extensions.Logging;
using EPAY.API.DTO;
using EPAY.Core;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using AutoMapper;
using EPAY.API.Helpers;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace EPAY.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize]
    public class BanksController : ControllerBase
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger<PaymentsController> _logger;
        private readonly ApiLogManager _apiLogManager;

        public BanksController(IUnitOfWork uow, ILogger<PaymentsController> logger, ApiLogManager logManager)
        {
            _uow = uow;
            _logger = logger;
            _apiLogManager=logManager;
        }


        [HttpGet("{ifu}")]
        public async Task<IActionResult> Get(string ifu)
        {
            try
            {
                var companyBanks=await _uow.TransferOrders.GetAll().Include(t=>t.Bank).Where(t=>t.Company.Ifu.Equals(ifu)).Select(t=>t.Bank).ToListAsync();
                if(companyBanks==null)
                {
                    var errorResponse=$"Company with ifu {ifu} does not have any transfer orders";
                    await _apiLogManager.CreateLogAsync(ifu, errorResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);
                    return NotFound(errorResponse);
                }
                var banksResponse=MapBankResponseFrom(companyBanks);
                await _apiLogManager.CreateLogAsync(ifu, banksResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.OK.ToString(),User.Identity.Name);
                return Ok(banksResponse);
            }
            catch (Exception ex)
            {
                await _apiLogManager.CreateLogAsync(ifu, ex, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);
                return BadRequest(ex);
            }
        }

        private List<GetCompanyBankResponse> MapBankResponseFrom(List<Bank> companyBanks)
        {
            var result=new List<GetCompanyBankResponse>();
            foreach (var b in companyBanks)
            {
                result.Add(new GetCompanyBankResponse{Id=b.Id,Logo=b.Logo,Name=b.Name});
            }
            return result;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                _logger.LogInformation("Getting bank list");
                var banks=await _uow.Banks.GetAll().ToListAsync();
                var banksResponse=MapBankResponseFrom(banks);
                await _apiLogManager.CreateLogAsync(null, banksResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.OK.ToString(),User.Identity.Name);               
                return Ok(banksResponse);
            }
            catch (System.Exception ex)
            {
                await _apiLogManager.CreateLogAsync(null, ex, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);
                return BadRequest(ex);
            }
        }
    }
}