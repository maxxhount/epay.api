using Microsoft.AspNetCore.Mvc;
using EPAY.Data;
using EPAY.Core.Models;
using Microsoft.Extensions.Logging;
using EPAY.API.DTO;
using EPAY.Core;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using EPAY.API.Helpers;
using System.Net;
using EPAY.Services;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Globalization;

namespace EPAY.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize]
    public class PaymentsController : ControllerBase
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger<PaymentsController> _logger;
        private readonly ApiLogManager _apiLogManager;
        private readonly INotificationService _emailService;
        private readonly IConfiguration _config;

        public PaymentsController(IUnitOfWork uow, ApiLogManager apiLogManager, IConfiguration configuration, INotificationService sendgridService, ILogger<PaymentsController> logger)
        {
            _uow = uow;
            _logger = logger;
            _apiLogManager=apiLogManager;
            _emailService=sendgridService;
            _config=configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Post(InitiatePaymentData paymentData)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var bank = await _uow.Banks.FirstOrDefaultAsync(b=>b.Id==paymentData.BankId);
                var IsDoublon=await IsExistingPayment(paymentData.CartId);
                if(IsDoublon)
                {
                    var errorResponse=$"Payment {paymentData.CartId} already exists";
                    await _apiLogManager.CreateLogAsync(paymentData, errorResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);
                    return BadRequest(errorResponse);
                }
                if (bank == null)
                {
                    var errorResponse=$"Bank with id {paymentData.BankId} not found";
                    await _apiLogManager.CreateLogAsync(paymentData, errorResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);                   
                    return NotFound(errorResponse);
                }
                var company = await _uow.Companies.GetAll().Include(c=>c.TransferOrders).SingleOrDefaultAsync(c => c.Ifu == paymentData.IFU);
                if (company == null)
                {
                    var errorResponse=$"Company with id {paymentData.IFU} not found";
                    await _apiLogManager.CreateLogAsync(paymentData, errorResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);                  
                    return NotFound(errorResponse);
                }
                if (company.TransferOrders == null)
                {
                    var errorResponse=$"Company with id {paymentData.IFU} does'nt have any transfer orders";
                    await _apiLogManager.CreateLogAsync(paymentData, errorResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);            
                    return NotFound(errorResponse);
                }
                var order = company.TransferOrders.SingleOrDefault(t => t.BankId == paymentData.BankId);
                if (order == null)
                {
                    var errorResponse=$"No transfer order found for company {paymentData.IFU} in bank {paymentData.BankId}";
                    await _apiLogManager.CreateLogAsync(paymentData, errorResponse, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.BadRequest.ToString(),User.Identity.Name);                    
                    return NotFound(errorResponse);
                }
                var declaration = GetDeclarationFrom(paymentData);

                declaration.AccountToDebit = order.AccountNumber;
                declaration.CompanyId = company.Id;
                declaration.BankId = bank.Id;
                declaration.Status = DeclarationStatus.PENDING;
                declaration.AccountToDebit=order.AccountNumber;
                await _uow.Declarations.AddAsync(declaration);
                await _uow.CommitAsync();
                var response=new { Id = declaration.Id, CartId = declaration.CartId, Status = declaration.Status, Amount = declaration.Amount, BankName=bank.Name, CreatedAt=declaration.CreatedDate, ApprovedAt=declaration.ValidationDate};
                await _apiLogManager.CreateLogAsync(paymentData, response, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.OK.ToString(),User.Identity.Name);                  
                await SendEmailNotificationToBankAsync(company.Email,bank.Email,company.Nom,company.Ifu,declaration.AccountToDebit,paymentData.CartId,paymentData.Amount,declaration.Status,bank.Id,bank.Name);
                return Ok(response);
            }
            catch (System.Exception ex)
            {
                _logger.LogError("",ex);
                return BadRequest(ex);
            }
        }

        [HttpGet("{cartId}")]
        public async Task<IActionResult> Get(string cartId)
        {
            try
            {
                _logger.LogInformation($"Getting payment status {cartId}");
                var d = await _uow.Declarations.GetAll().Include(d=>d.Bank).Include(d=>d.Company).FirstOrDefaultAsync(d => d.CartId.Equals(cartId));
                if (d == null)
                    return NotFound($"Payment with id {cartId} not found");
                return Ok(new GetPaymentResponse { Id = d.Id, CartId = d.CartId,IFU=d.Company?.Ifu, Status = d.Status, Amount = d.Amount, BankName=d.Bank?.Name, CompanyName=d.Company?.Nom, CreatedAt=d.CreatedDate, ProcessedAt=d.ValidationDate,AccountToDebit=d.AccountToDebit});
            }
            catch (Exception ex)
            {
                _logger.LogError("",ex);
                return BadRequest(ex);
            }
        }

        

        /* [HttpGet]
        [Route("list")]
        public async Task<IActionResult> GetPaymentsListByIFU([FromQuery]string ifu)
        {
            try
            {
                _logger.LogInformation($"Getting payments list for {ifu}");
                var declarations = await _uow.Declarations.GetAll().Include(d=>d.Bank).Include(d=>d.Company).Where(d => d.Company.Ifu.Equals(ifu)).ToListAsync();
                if (declarations == null)
                    return NotFound($"No payments found for company IFU: {ifu}");
                var response= BuildPaymentListResponse(declarations);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError("error GetPaymentsByIFU",ex);
                return BadRequest(ex);
            }
        } */

        [HttpGet]
        public async Task<IActionResult> GetPaymentsByIFU([FromQuery]string ifu)
        {
            try
            {
                _logger.LogInformation($"Getting payments list for {ifu}");
                var declarations = await _uow.Declarations.GetAll().Include(d=>d.Bank).Include(d=>d.Company).Where(d => d.Company.Ifu.Equals(ifu)).ToListAsync();
                if (declarations == null)
                    return NotFound($"No payments found for company IFU: {ifu}");
                var response= BuildPaymentListResponse(declarations);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError("error GetPaymentsByIFU",ex);
                return BadRequest(ex);
            }
        }

        [HttpGet("{from}/{to}/{status}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetPaymentsByBank([FromHeader]string bankEmail, string from, string to, string status)
        {
            try
            {
                if(string.IsNullOrEmpty(bankEmail))
                    return BadRequest("Bank user cannot be null");
                var bank= await GetBankFrom(bankEmail);
                if(bank==null)
                    return BadRequest("Bank user not found");
                _logger.LogInformation($"Getting {status} payments list for bank {bankEmail} from {from} to {to}");
                var declarations = _uow.Declarations.GetAll().Include(d=>d.Bank).Include(d=>d.Company).Include(d=>d.Company.TransferOrders).Where(d=>d.BankId==bank.Id);
                if (from != null)
                    declarations=declarations.Where(d=>d.CreatedDate.Value.Date>=GetDateFrom(from));
                if (to != null)
                    declarations=declarations.Where(d=>d.CreatedDate.Value.Date<=GetDateFrom(to));
                if (status != null)
                    declarations=declarations.Where(d=>d.Status.ToUpper()==status.Trim().ToUpper());
                var result=await declarations.ToListAsync();
                var response= BuildPaymentListResponse(result);
                return Ok(response);
            }
            catch (Exception ex)
            { 
                _logger.LogError("error GetPaymentsByBank",ex);
                return BadRequest(ex);
            }
        }

        private async Task<Bank> GetBankFrom(string bankEmail)
        {
            return await _uow.Banks.FirstOrDefaultAsync(b=>b.Email.Equals(bankEmail));
        }

        private DateTime GetDateFrom(string dateString)
        {
            var date= DateTime.ParseExact(dateString,"ddMMyyyy",CultureInfo.InvariantCulture);
            return date;
        }

        private async Task<TransferOrder> GetTransferOrderFor(int bankId, int companyId)
        {
            var order = await _uow.TransferOrders.FirstOrDefaultAsync(t => t.BankId == bankId && t.CompanyId== companyId);
            return order;
        }

        private List<GetPaymentResponse> BuildPaymentListResponse(List<Declaration> declarations)
        {
            var list= new List<GetPaymentResponse>();
            foreach (var d in declarations)
            {
                var r=new GetPaymentResponse()
                {
                    Id=d.Id,
                    Amount=d.Amount,
                    BankName=d.Bank.Name,
                    CartId=d.CartId,
                    CompanyName=d.Company.Nom,
                    CreatedAt=d.CreatedDate,
                    ProcessedAt=d.ValidationDate,
                    IFU=d.Company.Ifu,
                    Status=d.Status,
                    AccountToDebit=d.Company.TransferOrders.FirstOrDefault(t=>t.BankId==d.BankId)?.AccountNumber
                };
                list.Add(r);
            }
            return list;
        }

        private Declaration GetDeclarationFrom(InitiatePaymentData data)
        {

            var d = new Declaration()
            {
                CartId = data.CartId,
                Amount = data.Amount,
                CreatedDate = DateTime.Now,
                Status = DeclarationStatus.PENDING
            };
            return d;
        }

        private async Task<bool> IsExistingPayment(string cartId)
        {
            var dec= await _uow.Declarations.FirstOrDefaultAsync(d => d.CartId.ToLower().Equals(cartId.ToLower()));
            return dec != null;
        }

        

        private async Task SendEmailNotificationToBankAsync(string companyEmail,string bankEmail,string companyName,string companyIfu, string companyAccountNumber, string cartId, long amount, string status, int bankId,string bankName)
        {
            try
            {
                string[] ccEmails = {companyEmail,bankEmail};
                var bankOfficersEmails = await GetBankOfficerEmails(bankId);
                var api_key=  _config["email:apikey"];
                var subject = _config["email:subject"];
                var senderEmail = _config["email:senderEmail"];
                var senderName = _config["email:senderName"];
                var body = _config["email:body"];
                string formatedBody = string.Format(body,cartId, companyName,companyIfu, amount, companyAccountNumber,bankName);
                await _emailService.SendAsync(api_key,senderEmail,senderName,companyEmail, subject, formatedBody, cc: ccEmails);
            }
            catch (Exception ex)
            {
                _logger.LogError("",ex);
            } 
        }

        private async Task<string[]> GetBankOfficerEmails(int? bankId)
        {
            var bankOfficersEmails = await _uow.Users.Where(u => u.BankId == bankId && u.Profil == Role.BANK_OFFICER && u.IsValidated==true).Select(u => u.Email).ToArrayAsync();
            return bankOfficersEmails;
        }
    }
}