using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EPAY.API.Helpers;
using EPAY.API.Models;
using EPAY.API.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace EPAY.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AuthController : ControllerBase
    {
        IConfiguration _config;
        ApiLogManager _logManager;
        ILogger _logger;
        private const int expirationSeconds = 360;

        public AuthController(IConfiguration config, ApiLogManager logManager, ILogger<AuthController> logger)
        {
            _config = config;
            _logManager = logManager;
        }

        [Route("token")]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateUserResource model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var user = FindApiUser(model.ClientId, model.ClientSecret);
                if (user == null)
                    return Unauthorized("User not found");
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub,user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Name,user.ClientId)
                };

                var token = GenerateToken(claims);

                var response = new OAuthResponse
                {
                    Token = token,
                    ExpireDate = expirationSeconds
                };
                //await SaveTokenFor(user.Id, token);
                await _logManager.CreateLogAsync(model, response, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.OK.ToString(),model.ClientId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                await _logManager.CreateLogAsync(model, ex, Request.Host.Host, Request.Path.Value, Request.Method, HttpStatusCode.InternalServerError.ToString(),model.ClientId);
                return BadRequest(ex);
            }
        }

        private ApiUser FindApiUser(string id, string secret)
        {
            var user = GetApiUserFromConfiguration();
            if (user.ClientId == id && user.ClientSecret==secret)
                return user;
            return null;
        }

        private ApiUser GetApiUserFromConfiguration()
        {
            var clientId = _config["apiUser:ClientId"];
            var clientSecret = _config["apiUser:ClientSecret"];
            return new ApiUser{Id=1,ClientId=clientId,ClientSecret=clientSecret};
        }

        private string GenerateToken(IEnumerable<Claim> claims)
        {
            string signinKey = _config["apiSettings:signinKey"];
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signinKey));

            var jwt = new JwtSecurityToken(
                issuer: _config["apiSettings:issuer"],
                audience: _config["apiSettings:audience"],
                claims: claims,
                expires: DateTime.UtcNow.AddSeconds(expirationSeconds),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            );

            return new JwtSecurityTokenHandler().WriteToken(jwt); //the method is called WriteToken but returns a string
        }



        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            string signinKey = _config["apiSettings:signinKey"];
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signinKey)),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");
            return principal;
        }
    }
}