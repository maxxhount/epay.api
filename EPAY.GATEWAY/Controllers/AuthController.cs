﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EPAY.GATEWAY.Helpers;
using EPAY.GATEWAY.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EPAY.GATEWAY.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class AuthController : Controller
    {

        private readonly ILogger<AuthController> _logger;
        private readonly IMapper _mapper;

        public AuthController(ILogger<AuthController> logger, IMapper mapper)
        {
            _logger = logger;
            _mapper=mapper;
        }

        [Route("token")]
        [HttpPost]
        public IActionResult Login(LoginResource loginResource)
        {
            if(!ModelState.IsValid)
                return BadRequest(new {Success=false, Errors=ModelStateValidator.GetErrors(ModelState)});
            return Ok(new {Success=true,Token=Guid.NewGuid().ToString("N")});
        }
    }
}
