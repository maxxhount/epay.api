﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EPAY.GATEWAY.Helpers;
using EPAY.GATEWAY.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EPAY.GATEWAY.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class TransactionsController : Controller
    {

        private readonly ILogger<TransactionsController> _logger;
        private readonly IMapper _mapper;

        public TransactionsController(ILogger<TransactionsController> logger, IMapper mapper)
        {
            _logger = logger;
            _mapper=mapper;
        }

        [Route("create")]
        [HttpPost]
        public IActionResult Create(CreateTransactionResource createTransactionResource)
        {
            if(!ModelState.IsValid)
                return BadRequest(new {Success=false, Errors=ModelStateValidator.GetErrors(ModelState)});
            var transaction= _mapper.Map<TransactionResource>(createTransactionResource);
            transaction.Reference=ReferenceProvider.Generate();
            transaction.Id=Guid.NewGuid().ToString("N");
            return Ok(new {Success=true,Transaction=transaction});
        }

        [Route("pay/{reference}")]
        [HttpGet]
        public IActionResult Execute(string reference)
        {
            return Content("Index payment page");
        }

        [Route("{reference}")]
        [HttpGet]
        public IActionResult Get(string reference)
        {
            return Ok(new {Success=true,Transaction=GetDummyTransaction()});
        }


        private TransactionResource GetDummyTransaction()
        {
            return new TransactionResource()
            {
                Amount=580000,
                Callback_Url="https://mystore.com",
                Currency="XOF",
                Description="DGI IMPOTS REF 5454844545121",
                Reference=ReferenceProvider.Generate(),
                Id=Guid.NewGuid().ToString("N"),
                Status="APPROVED"
            };
        }
    }
}
