using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace EPAY.GATEWAY.Helpers
{
    public class ModelStateValidator
    {
        public static List<ModelError> GetErrors(ModelStateDictionary modelState)
        {
            var errors = modelState
                .Where(a => a.Value.Errors.Count > 0)
                .SelectMany(x => x.Value.Errors)
                .ToList();
            return errors;
        }
    }
}