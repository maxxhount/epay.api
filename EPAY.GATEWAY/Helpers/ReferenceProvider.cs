using System;

namespace EPAY.GATEWAY.Helpers
{
    public class ReferenceProvider
    {
        private static char[] letters={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        private static Random random=new Random();
        public static string Generate()
        {
            var id=Guid.NewGuid().ToString("N").Substring(0,8);
            var prefixLetters=GetThreeRandomLetters();
            return $"{prefixLetters}-{id}".ToUpper();
        }

        private static string GetThreeRandomLetters()
        {
            string randomString="";
            for(int i=0;i<3;i++)
            {
                var index=random.Next(0,letters.Length-1);
                randomString+=letters[index];
            }
            return randomString;
        }
    }
}