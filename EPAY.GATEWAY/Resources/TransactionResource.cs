namespace EPAY.GATEWAY.Resources
{
    public class TransactionResource
    {
        public string Id { get; set; }
        public string Reference { get; set; }
        public long Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; } 
        public string Callback_Url { get; set; }
        public string Status { get; set; }="PENDING";

    }
}