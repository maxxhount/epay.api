using System.ComponentModel.DataAnnotations;

namespace EPAY.GATEWAY.Resources
{
    public class CreateTransactionResource
    {
        [Required] 
        public CustomerResource Customer { get; set; }
        [Required]
        public long Amount { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string Description { get; set; }
        public string Callback_url { get; set; }
    }
}