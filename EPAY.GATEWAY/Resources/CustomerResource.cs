using System.ComponentModel.DataAnnotations;

namespace EPAY.GATEWAY.Resources
{
    public class CustomerResource
    {

        [Required]  
        public string Name { get; set; }
        public string Id { get; set; }
        [Required] 
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Required] 
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}