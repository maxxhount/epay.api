using AutoMapper;
using EPAY.GATEWAY.Resources;

namespace EPAY.GATEWAY.Mappings
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateTransactionResource,TransactionResource>();
        }
    }
}