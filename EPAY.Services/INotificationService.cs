﻿using System;
using System.Threading.Tasks;

namespace EPAY.Services
{
    public interface INotificationService
    {
        Task SendAsync(string api_key, string senderEmail,string senderName, string to, string subject, string body, string[] cc = null, bool isHtml = true);
        Task SendAsync(string api_key, string senderEmail,string senderName, string[] tos, string subject, string body, string[] cc = null, bool isHtml = true);
    }
}
