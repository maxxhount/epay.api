﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace EPAY.Services
{
    public class SendGridService : INotificationService
    {
        private string API_KEY = "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";
        ILogger _logger;
        public SendGridService(ILogger<SendGridService> logger)
        {
            _logger = logger;
        }
        public async Task SendAsync(string api_key, string senderEmail,string senderName, string to, string subject, string body, string[] cc = null, bool isHtml = true)
        {
            try
            {
                SetAPIKeyValue(api_key);
                var msg = new SendGridMessage();
                var client = new SendGridClient(API_KEY);
                var from = new EmailAddress(senderEmail, senderName);
                var destination = new EmailAddress(to);
                if (isHtml == true)
                    msg = MailHelper.CreateSingleEmail(from, destination, subject, "", body);
                else
                    msg = MailHelper.CreateSingleEmail(from, destination, subject, body, "");
                msg.AddCategory("TELEPAIEMENT");
                if (cc != null)
                    msg.AddCcs(GetEmailAddressListFrom(cc));
                var response = await client.SendEmailAsync(msg);
                _logger.LogWarning("Email input  {response}", JsonConvert.SerializeObject(msg));
                _logger.LogWarning("Email response {response}", JsonConvert.SerializeObject(response));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex,"Email error");
            }
        }

        public async Task SendAsync(string api_key, string senderEmail,string senderName, string[] tos, string subject, string body, string[] cc = null, bool isHtml = true)
        {
            try
            {
                SetAPIKeyValue(api_key);
                var msg = new SendGridMessage();
                var client = new SendGridClient(API_KEY);
                var from = new EmailAddress(senderEmail, senderName);
                var destination = GetEmailAddressListFrom(tos);
                if (isHtml == true)
                    msg = MailHelper.CreateSingleEmailToMultipleRecipients(from,destination,subject,"",body);
                else
                    msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, destination, subject, body,"");
                msg.AddCategory("TELEPAIEMENT");
                if (cc != null)
                    msg.AddCcs(GetEmailAddressListFrom(cc));
                var response = await client.SendEmailAsync(msg);
                var code = response.StatusCode;
               _logger.LogWarning("Email input  {response}", JsonConvert.SerializeObject(msg));
                _logger.LogWarning("Email response {response}", JsonConvert.SerializeObject(response));
            }
            catch (System.Exception ex)
            {
                _logger.LogError("Email error", ex);
            }
        }

        private void SetAPIKeyValue(string key)
        {
            if (string.IsNullOrEmpty(key))
                return;
            API_KEY = key;
        }
        private List<EmailAddress> GetEmailAddressListFrom(string[] cc)
        {
            var list = new List<EmailAddress>();           
            if (cc != null)
            {
                cc= cc.Distinct().ToArray();
                for (int i = 0; i < cc.Length; i++)
                {
                    list.Add(new EmailAddress(cc[i]));
                }
            }
            return list;
        }
    }
}
