using System;
using System.Collections.Generic;

namespace EPAY.Core.Models
{
    public class AspNetUser
    {
        public AspNetUser()
        {
            Declarations = new HashSet<Declaration>();
        }

        public string Id { get; set; }
        public string Nom { get; set; }
        public string Prenoms { get; set; }
        public string TempPassword { get; set; }
        public string Ip { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool IsValidated { get; set; }
        public DateTime? LastConnectionDate { get; set; }
        public string Profil { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public int? BankId { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual ICollection<Declaration> Declarations { get; set; }
    }

    public class Role
    {
        public static string BANK_OFFICER { get { return "BANK_OFFICER"; } }
        public static string BANK_AUDITOR { get { return "BANK_AUDITOR"; } }
        public static string SUPER_ADMIN { get { return "SUPER_ADMIN"; } }
        public static string DGI_ADMIN { get { return "DGI_ADMIN"; } }
        public static string DGI_USER { get { return "DGI_USER"; } }
        public static string OPERATION_OFFICER { get { return "MAIN_BANK_VALIDATOR"; } }
    }
}