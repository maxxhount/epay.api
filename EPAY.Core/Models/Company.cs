﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EPAY.Core.Models
{
    public class Company
    {
        public Company()
        {
            Declarations = new HashSet<Declaration>();
            TransferOrders = new HashSet<TransferOrder>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Ifu { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string TransferNumber { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual ICollection<Declaration> Declarations { get; set; }
        public virtual ICollection<TransferOrder> TransferOrders { get; set; }
    }
}