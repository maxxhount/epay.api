using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EPAY.Core.Models
{
    public class TransferOrder
    {
        public string Id { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CompanyId { get; set; }
        public string AddedBy { get; set; }
        public int? BankId { get; set; }
        public string Reference { get; set; }
        public string Url { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual Company Company { get; set; }
    }
}