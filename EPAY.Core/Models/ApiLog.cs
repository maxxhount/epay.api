using System;

namespace EPAY.Core.Models
{
    public class ApiLog
    {
        public ApiLog()
        {
            Id=Guid.NewGuid().ToString("N");
        }
        public string Id { get; set; }
        public string Method { get; set; }
        public string Url { get; set; }
        public string Status { get; set; }
        public string ClientIP { get; set; }
        public string Input { get; set; }
        public string Response { get; set; }
        public DateTime? AddedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
        public string AddedBy { get; set; }
        public string DeletedBy { get; set; }
        public bool? IsDeleted { get; set; }
    }
}