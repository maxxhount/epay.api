﻿using System;
using EPAY.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EPAY.Data
{
    public class ApplicationDbContext:DbContext
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Declaration> Declarations { get; set; }
        public DbSet<TransferOrder> TransferOrders { get; set; }
        public DbSet<AspNetUser> AspNetUsers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {                     
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<AspNetUser>(entity =>
            {
                entity.HasIndex(e => e.BankId)
                    .HasName("IX_Bank_Id");
                entity.HasIndex(e => e.UserName)
                    .HasName("UserNameIndex")
                    .IsUnique();

                entity.Property(e => e.Id).HasMaxLength(128);

                entity.Property(e => e.BankId).HasColumnName("Bank_Id");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.Ip).HasColumnName("IP");

                entity.Property(e => e.LastConnectionDate).HasColumnType("datetime");

                entity.Property(e => e.LockoutEndDateUtc).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.AspNetUsers)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("FK_dbo.AspNetUsers_dbo.Banks_Bank_Id");
            });

            

            modelBuilder.Entity<Bank>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Ifu).HasColumnName("IFU");
            });

            modelBuilder.Entity<Declaration>(entity =>
            {
                entity.HasIndex(e => e.BankId)
                    .HasName("IX_Bank_Id");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("IX_Company_Id");

                entity.HasIndex(e => e.ValidatorId)
                    .HasName("IX_Validator_Id");

                entity.Property(e => e.Id).HasMaxLength(128);

                entity.Property(e => e.BankId).HasColumnName("Bank_Id");

                entity.Property(e => e.CompanyId).HasColumnName("Company_Id");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValidationDate).HasColumnType("datetime");

                entity.Property(e => e.ValidatorId)
                    .HasColumnName("Validator_Id")
                    .HasMaxLength(128);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.Declarations)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("FK_dbo.Declarations_dbo.Banks_Bank_Id");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Declarations)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_dbo.Declarations_dbo.Companies_Company_Id");

                entity.HasOne(d => d.Validator)
                    .WithMany(p => p.Declarations)
                    .HasForeignKey(d => d.ValidatorId)
                    .HasConstraintName("FK_dbo.Declarations_dbo.AspNetUsers_Validator_Id");
            });


            modelBuilder.Entity<TransferOrder>(entity =>
            {
                entity.HasIndex(e => e.BankId)
                    .HasName("IX_Bank_Id");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("IX_Company_Id");

                entity.Property(e => e.Id).HasMaxLength(128);

                entity.Property(e => e.BankId).HasColumnName("Bank_Id");

                entity.Property(e => e.CompanyId).HasColumnName("Company_Id");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.TransferOrders)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("FK_dbo.TransferOrders_dbo.Banks_Bank_Id");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.TransferOrders)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_dbo.TransferOrders_dbo.Companies_Company_Id");
            });

            base.OnModelCreating(modelBuilder);
        }

        

    }
}
