using System.Threading.Tasks;
using EPAY.Core.Models;
using EPAY.Data;
using EPAY.Data.Repositories;

namespace EPAY.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private  IRepository<Company> _companyRepository;
        private  IRepository<Bank> _bankRepository;
        private  IRepository<Declaration> _declarationRepository;
        private  IRepository<TransferOrder> _transferOrderRepository;
        private  IRepository<AspNetUser> _userRepository;
       

        public UnitOfWork(ApplicationDbContext context)
        {
            _context=context;
        }
        public IRepository<Company> Companies => _companyRepository=_companyRepository??new Repository<Company>(_context);
        public IRepository<AspNetUser> Users => _userRepository=_userRepository??new Repository<AspNetUser>(_context);

        public IRepository<Bank> Banks => _bankRepository=_bankRepository??new Repository<Bank>(_context);

        public IRepository<Declaration> Declarations => _declarationRepository=_declarationRepository??new Repository<Declaration>(_context);
        public IRepository<TransferOrder> TransferOrders => _transferOrderRepository=_transferOrderRepository??new Repository<TransferOrder>(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}