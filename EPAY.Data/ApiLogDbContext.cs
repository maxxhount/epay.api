using EPAY.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace EPAY.Data
{
    public class ApiLogDbContext:DbContext
    {
        public DbSet<ApiLog> ApiLogs { get; set; }

        public ApiLogDbContext(DbContextOptions<ApiLogDbContext> options):base(options)
        {                     
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}