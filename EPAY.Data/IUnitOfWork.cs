using System.Threading.Tasks;
using EPAY.Core.Models;
using EPAY.Data.Repositories;

namespace EPAY.Core
{
    public interface IUnitOfWork
    {
        IRepository<Company> Companies { get; }
        IRepository<Bank> Banks { get; }
        IRepository<Declaration> Declarations { get; }
        IRepository<TransferOrder> TransferOrders { get; }
        
        IRepository<AspNetUser> Users { get; }
        Task<int> CommitAsync();
    }
}